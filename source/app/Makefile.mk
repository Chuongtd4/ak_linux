-include source/app/task/Makefile.mk

CXXFLAGS	+= -I./source/app

VPATH += source/app

OBJ += $(OBJ_DIR)/app.o
OBJ += $(OBJ_DIR)/app_data.o
OBJ += $(OBJ_DIR)/app_config.o
OBJ += $(OBJ_DIR)/task_list.o

OBJ += $(OBJ_DIR)/time_lapse.o

